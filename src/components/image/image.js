import React from 'react';
import image from './top-img.png';
import './image.css';

const Img=()=>{
    return (
        <img src={image} alt="Top_Img"/>
    )
}

export default Img