import React from 'react';
import NavMenu from '../navmenu/navmenu.js';
import Img from '../image/image.js';
import Ttl from '../title/title.js';
import Txt1 from '../text1/text1.js';
import Txt2 from '../text2/text2.js';
import Btn from '../button/button.js';
import End from '../end/end.js';
import './app.css';

const App=()=>{
        return (
                <div className="site">
                        <div className="container">
                                <NavMenu/>
                        </div>

                        <div className="top">
                                <Img/>
                        </div>

                        <div className="header">
                                <div className="title">
                                        <Ttl/>
                                </div>
                                <div className="text1">
                                        <Txt1/>
                                </div>
                                <div className="text2">
                                        <Txt2/>
                                </div>
                                <div className="button">
                                        <Btn/>
                                </div>
                        </div>

                        <div className="footer">
                                <End/>
                        </div>
                </div>
        )
}

export default App